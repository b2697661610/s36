// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://mervin03091996:admin123@zuitt-bootcamp.cljzexh.mongodb.net/s35?retryWrites=true&w=majority", 
	{ 
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);
mongoose.connection.once("open", () => console.log('Now connected to the database!'));

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);


app.get("/all-task", (req, res) => {
	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {
		// If an error occurred
		if(err) {
			// Will print any errors found in the console
			return console.log(err);
		  // If no errors are found
		} else {
			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			return res.status(200).json({
				data: result
			})
		}
	})
});


app.get("tasks/", (req, res) => {
	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {
		// If an error occurred
		if(err) {
			// Will print any errors found in the console
			return console.log(err);
		  // If no errors are found
		} else {
			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			return res.status(200).json({
				data: result
			})
		}
	})
});


const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "complete"
	}
});


// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

